<?php

class Employee_model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

    /* @param: employee name , date
     * @description: function to fetch employee expenses
     * @author: gowithexpert
     * @copyrights: 
     */

  public function emp_data($month, $year) {

    $this->selectedMonth = $month;
    $this->selectedYear = $year;
    $this->db->select('*');
    $this->db->from('emp_expenses');
    $this->db->where('email', $this->session->userdata('email'));
    $this->db->where("date LIKE '$this->selectedMonth%$this->selectedYear' OR date LIKE '0$this->selectedMonth%$this->selectedYear'");
    $this->query = $this->db->get();
    if ($this->query->result_array()) { 
      return $this->query->result_array();
    } 
    else {
      return array(); //return blank array
    }
  } // the function ends here 

} // the end of class
