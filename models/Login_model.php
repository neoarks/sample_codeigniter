<?php  

 class Login_model extends CI_Model {
       
       
     /* @param: Class constructor
      * @description: Auto intialized on object creation
      * @author: gowithexpert
      * @copyrights: 
      */ 

      public function __construct() {
        $this->load->database();
      } 
      

     /* @param: Function name: emp_login($email, $pwd)
      * @description: Function has used for employees login
      * @author: gowithexpert
      * @copyrights: 
      */ 

    public function emp_login( $email, $pwd ) {
      //Initialised login variables.
      $this->email = $email;
      $this->pwd = $pwd;

      $this->db->select('*');
      $this->db->from('sis_employees');
      $this->db->where('emp_email', $this->email);
      $this->db->where('emp_pwd', $this->pwd);
      $this->query = $this->db->get();
      if($this->query->result_array()) {
          return $this->query->result_array();
      }
      else {
        return array(); //return blank array
      }
    } // Function end here 



     /* @param: Function name: psvFileinsertion($psvdata)
      * @description: Function has used for Employees Registration
      * @author: gowithexpert
      * @copyrights: 
      */ 

    public function psvFileinsertion($psvdata) {
      $this->psvfileData = $psvdata;
      if($this->db->insert('emp_expenses' , $this->psvfileData)) {
        return true;
      }
      else {
        return false;
      }

  } // the function ends here 

} // the end of class
      