<script src="<?php echo base_url();?>vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url();?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>vendor/select2/select2.min.js"></script>
	<script src="<?php echo base_url();?>vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<script src="<?php echo base_url();?>js/main.js"></script>

<!-- Registeration Form -->
 
    <!-- Jquery JS-->
    <script src="<?php echo base_url();?>vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="<?php echo base_url();?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/datepicker/moment.min.js"></script>
    <script src="<?php echo base_url();?>vendor/datepicker/daterangepicker.js"></script>
    <!-- Main JS-->
    <script src="<?php echo base_url();?>js/global.js"></script>

    <!-- flash messages -->
    <script src="<?php echo base_url();?>js/toastr.min.js"></script>
	<script type="text/javascript">

	<?php if($this->session->flashdata('success')){ ?>
	    toastr.success("<?php echo $this->session->flashdata('success'); ?>");
	<?php }else if($this->session->flashdata('error')){  ?>
	    toastr.error("<?php echo $this->session->flashdata('error'); ?>");
	<?php }else if($this->session->flashdata('warning')){  ?>
	    toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
	<?php }else if($this->session->flashdata('info')){  ?>
	    toastr.info("<?php echo $this->session->flashdata('info'); ?>");
	<?php } ?>


</script>