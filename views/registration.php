<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once('header.php');?>
</head>
<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
         <div class="card card-1">

                <div class="card-body">
                    <h4 align="right"><a href="<?php echo base_url('/'); ?>" style= "text-decoration: none;" > Login </a> </h4>
                    <h2 class="title">Registration </h2> 
                   
                       <!-- <form action="" method="POST">-->
                        <form class="regis100-form validate-form" action="<?php echo base_url()?>index.php/Registration/emp_registration" method="post">
                            
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group" data-validate = "First name is required">
                                    <input class="input--style-1" type="text" placeholder="First name" name="first_name">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group" data-validate = "Last name is required">
                                    <input class="input--style-1" type="text" placeholder="Last name" name="last_name">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group" data-validate = "Birth_date is required">
                                    <input class="input--style-1 js-datepicker" type="text" placeholder="Birth_date" name="birthday">
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group" data-validate = "Mobile is required">
                                    <input class="input--style-1" type="text" placeholder="Mobile" name="mob">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group" data-validate = "Email is required">
                                    <input class="input--style-1" type="text" placeholder="Email" name="email">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group" data-validate = "Password is required">
                                    <input class="input--style-1" type="password" placeholder="Password" name="password">
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="dept">
                                    <option disabled="disabled" selected="selected">Department</option>
                                    <option value="0">HRD</option>
                                    <option value="1">SALES Department</option>
                                    <option value="2">IT Department</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        <div class="p-t-20">
                            <button class="login100-form-btn" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer-->
    <?php require_once('footer.php');?>

</body>
</html>
<!-- end document-->
