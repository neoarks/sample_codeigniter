<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<?php require_once('header.php'); ?>

</head>
<body>

	<div class="limiter">
		<div align="right"><a href="<?php echo base_url('index.php'); ?>"> Logout </a></div>
		<?php echo $this->session->tempdata('admin_success'); ?>
		<div class="container-login100">

			<div class="wrap-login100" id="wrapper_fileupload">
				<!--<form enctype="multipart/form-data" method="post" action="http://localhost/SIS_Project/index.php/Home/psv_upload" role="form">-->
				<form enctype="multipart/form-data" method="post" action="Home/psv_upload" role="form">
				<div class="form-group">
				<label for="exampleInputFile">File Upload</label>
				<input type="file" name="file" id="file" size="150">
				<p class="help-block">Only PSV File Import.</p>
				</div>
				<button type="submit" class="login100-form-btn" name="submit" value="Import">Upload</button>
				</form>
			</div>
		</div>
	</div>
	
	<?php require_once('footer.php'); ?>

</body>
</html>
