<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Header Section -->
    <?php require_once('header.php'); ?>
</head>

<body>
    <div align="right"><a href="<?php echo base_url('index.php/Login/logout'); ?>"> Logout </a></div>
    <?php echo $this->session->tempdata('emp_success'); ?>

    <div id="dateForm" class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">

        <div class="wrapper wrapper--w680">

            <div class="card card-1">

                <div class="card-body">

                    <h2 id="title" class="title">Employee </h2>
                    <form action="<?php echo base_url() ?>index.php/Employee/emp_expense" method="POST">

                        <div class="row row-space">
                            <div class="col-2 rs-select2 js-select-simple select--no-search">
                                <select name="year">
                                    <option disabled="disabled" selected="selected">Select Year</option>
                                    <option value="2019">2019</option>
                                    <option value="2018">2018</option>
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>

                            <div class="col-2 rs-select2 js-select-simple select--no-search">
                                <select name="date">
                                    <option disabled="disabled" selected="selected">SELECT MONTH</option>
                                    <option value="1">JANUARY</option>
                                    <option value="2">FEBRUARY</option>
                                    <option value="3">MARCH</option>
                                    <option value="4">APRIL</option>
                                    <option value="5">MAY</option>
                                    <option value="6">JUNE</option>
                                    <option value="7">JULY</option>
                                    <option value="8">AUGUST</option>
                                    <option value="9">SEPTEMBER</option>
                                    <option value="10">OCTOBER</option>
                                    <option value="11">NOVEMBER</option>
                                    <option value="12">DECEMBER</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        <div class="p-t-20">
                            <!--<button class="btn btn--radius btn--green" type="submit">Submit</button>-->
                            <button class="login100-form-btn" type="submit">Submit</button>
                        </div>

                    </form>

                </div>
            </div>

            <div class="box-header table-responsive">
                <?php $this->session->set_flashdata('success', 'Expenses records list'); ?>

                <table id="example1" class="table table-bordered table-striped" style="width: 100%">
                    <thead style="border:1px solid black;">
                        <th style="text-align:center;">Employee name</th>
                        <th style="text-align:center;">Employee address</th>
                        <th style="text-align:center;">Date</th>
                        <th style="text-align:center;">Category</th>
                        <th style="text-align:center;">Expense description</th>
                        <th style="text-align:center;">Pre tax amount</th>
                        <th style="text-align:center;">Tax amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($this->emp_arr as $this->row) {
                            ?>
                            <tr>
                                <td style="text-align:center;"><?php echo $this->row['employee_name']; ?></td>
                                <td style="text-align:center;"><?php echo $this->row['employee_address']; ?></td>
                                <td style="text-align:center;"><?php echo $this->row['date']; ?></td>
                                <td style="text-align:center;"><?php echo $this->row['category']; ?></td>
                                <td style="text-align:center;"><?php echo $this->row['expense_description']; ?></td>
                                <td style="text-align:center;"><?php echo $this->row['pre_tax_amount']; ?></td>
                                <td style="text-align:center;"><?php echo $this->row['tax_amount']; ?></td>
                            </tr>
                        <?php

                    }
                    ?>

                    </tbody>

                </table>

            </div>


        </div>
    </div>
    <!-- Footer Section -->
    <?php require_once('footer.php'); ?>

</body>

</html>
<!-- end document-->