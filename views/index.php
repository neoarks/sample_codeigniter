<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<!-- Header -->
	<?php require_once('header.php');?>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo base_url()?>images/img-01.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="<?php echo base_url()?>index.php/Login/Emp_login" method="post">
					<!--<form class="login100-form validate-form" action="" method="post">-->
					<p class="" style="text-align: center; ">
						<span>Login</span>
					</p>
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: myemail@gmail.com">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pwd" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="submit" value="submit">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forget
						</span>
						<a class="txt2" href="?info=1">
							Username / Password?
						</a>
						
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="<?php echo base_url()?>index.php/registration/">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
						
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Footer-->
	<?php require_once('footer.php');?>
</body>
</html>
