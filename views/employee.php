<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <?php require_once('header.php'); ?>
</head>

<body>
    <div align="right"><a href="<?php echo base_url('index.php/Login/logout'); ?>"> Logout </a></div>
    <?php echo $this->session->tempdata('emp_success'); ?>

    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">

        <div class="wrapper wrapper--w680">

            <div class="card card-1">

                <div class="card-body">

                    <h2 class="title">Expenses </h2>
                    <form action="<?php echo base_url() ?>index.php/Employee/emp_expense" method="POST">
                        <!-- <form action="#" method="POST">-->
                        <div class="row row-space">
                            <!-- Year Selection -->
                            <div class="col-2 rs-select2 js-select-simple select--no-search">
                                <select name="year">
                                    <option disabled="disabled" selected="selected">Select Year</option>
                                    <option value="2019">2019</option>
                                    <option value="2018">2018</option>
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>

                            <!-- Month Selection -->
                            <div class="col-2 rs-select2 js-select-simple select--no-search">
                                <select name="month">
                                    <option disabled="disabled" selected="selected">Select Month</option>
                                    <option value="1">JANUARY</option>
                                    <option value="2">FEBRUARY</option>
                                    <option value="3">MARCH</option>
                                    <option value="4">APRIL</option>
                                    <option value="5">MAY</option>
                                    <option value="6">JUNE</option>
                                    <option value="7">JULY</option>
                                    <option value="8">AUGUST</option>
                                    <option value="9">SEPTEMBER</option>
                                    <option value="10">OCTOBER</option>
                                    <option value="11">NOVEMBER</option>
                                    <option value="12">DECEMBER</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>

                        <div class="p-t-20">
                            <!--<button class="btn btn--radius btn--green" type="submit">Submit</button>-->
                            <button class="login100-form-btn" type="submit">Find Records</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <?php require_once('footer.php'); ?>

</body>

</html>
<!-- end document-->