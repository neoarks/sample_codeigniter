<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


     /* @param: Class constructor
      * @description: Auto intialized on object creation
      * @author: gowithexpert
      * @copyrights: 
      */

  	public function __construct() {
   
  		parent::__construct();
  		$this->load->model('Home_model');
  		if ( $this->session->userdata('user_name') === null ) { 
        $allowed = array('index'); // Allowed registration function without login?
        if ( ! in_array($this->router->fetch_method(), $allowed )) {
          $this->session->set_flashdata('waring', 'Login requried'); 
          redirect('login');
        }
      }
  	} 



	 /* @param: Index Function 
      * @description: Function is used for rendering index (view html) 
      * @author: gowithexpert
      * @copyrights: 
      */ 
	 
	public function index() {

		$this->load->view('Home');
	}


     /* @param: Upload PSV file 
      * @description:  function for upload PSV file by the Admin
      * @author: gowithexpert
      * @copyrights: 
      */ 

    public function psv_upload() {
        $this->rowCount = 0;
        // If import request is submitted
        if($this->input->post('submit')) {
          //If file uploaded
        	$this->insertCount = 0;
          if(is_uploaded_file($_FILES['file']['tmp_name'])) {         
            $this->PSVfile = $this->parse_psv($_FILES['file']['tmp_name']);
					 //Insert/update CSV data into database
            if(!empty($this->PSVfile)) {
              foreach($this->PSVfile as $row) {
                $this->rowCount++;
                $this->insert = $this->Home_model->psvFileinsertion($row);
                if($this->insert) {
                  $this->insertCount++;
                }
              }
              $this->session->set_flashdata('success', "$this->insertCount" .' Records have inserted successfully');
              redirect('Home');
            }
            else {
              $this->session->set_flashdata('warning', 'Please browse psv file.');
              redirect('Home');
            }
          }
          else {
            $this->session->set_flashdata('warning', 'Please browse psv file.');
            redirect('Home');
          }
		    }
  } // the PSV file upload function close here 



     /* @param: Parsing psv file
      * @description: Parsing psv file for making compatible it with the database fields
      * @author: gowithexpert
      * @copyrights: 
      */ 

	function parse_psv($filepath) {

    	 $this->separator = '|';/** separator used to explode each line */
	     $this->enclosure = ' ';/** enclosure used to decorate each field */
	     $this->max_row_size = 4096;/** maximum row size to be used for decoding */
        
        // If file doesn't exist, return false
        if(!file_exists($filepath)) {
            return FALSE;            
        }
        // Open uploaded CSV file with read-only mode
        $this->csvFile = fopen($filepath, 'r');
        // Get Fields and values
        $this->fields = fgetcsv($this->csvFile, $this->max_row_size, $this->separator, $this->enclosure);
		    $this->keys = $this->escape_string($this->fields);
        // Store CSV data in an array
        $this->fieldTot = count($this->fields);
        $csvData = array();
        $i = 0;
        $this->psvArr[] = array();
        while(($row = fgetcsv($this->csvFile, $this->max_row_size, $this->separator, $this->enclosure)) !== FALSE) {
           if($row != NULL) {
                if(count($this->keys) == count($row)) {
                    $this->arr        = array();
                    $this->new_values = array();
                    $this->new_values = $this->escape_string($row);
                    for($this->j = 0; $this->j < count($this->keys); $this->j++){
                        if($this->keys[$this->j] != ""){
                            $this->arr[$this->keys[$this->j]] = $this->new_values[$this->j];
                        }
                    }
                    $this->csvData[$i] = $this->arr;
                    $i++;
                }
			     }
        } //while loop closed    
        fclose($this->csvFile); // Close opened CSV file
		    return $this->csvData;

      } // function closed



     /* @param: Escaping strings 
      * @description: Just for scaping string during parsing psv file
      * @author: gowithexpert
      * @copyrights: 
      */ 

     function escape_string($data) {
        $result = array();
        foreach($data as $row){
            $result[] = str_replace(' ', '', $row);
        }
        return $result;
    }   //close function
       
    
} // the end of class