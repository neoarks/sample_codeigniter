<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Employee extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('Employee_model');

    if ( $this->session->userdata('user_name') === null ) { 
          $allowed = array('index'); // Allowed registration function without login?
          if ( ! in_array($this->router->fetch_method(), $allowed )) {
            $this->session->set_flashdata('waring', 'Login requried'); 
            redirect('login');
          }
      }
  }

     /* @param: 
      * @description: index function to load employee view 
      * @author: 
      * @copyrights: 
      */

  public function index()
  {
    $this->load->view('employee');
  }


     /* @param: Search expenses 
      * @description: Function is useded for Employee expenses list
      * @author: gowithexpert
      * @copyrights: 
      */
  public function emp_expense() {
    //Initialized month and year variables retrieved from form
    $this->month = $this->input->post('month'); 
    $this->year = $this->input->post('year'); 
    $this->emp_name = $this->session->userdata['user_name'];

    //Server side validations
    if ($this->month == '' && $this->month == 0 && $this->month > 12 ) {
      $this->session->set_flashdata('warning', 'Invalid or blank month');
      redirect('employee');
    } 
    elseif ($this->year == '' ) {
      $this->session->set_flashdata('warning', 'Year can not blank');
      redirect('employee');
    } 
    elseif ($this->month == '' ) {
      $this->session->set_flashdata('warning', 'Month can not blank');
      redirect('employee');
    } 
    else {
      $this->emp_arr = $this->Employee_model->emp_data($this->month,  $this->year);
      $this->emp_tot = count($this->emp_arr);
      if ($this->emp_tot != 0 ) {
        $this->load->view('list', $this->emp_arr);
      } 
      else {
        $this->session->set_flashdata('info', 'No record found');
        redirect('employee'); /*echo "Employee data is not found";*/
      }
    } // end of else part 
  } // function closed 

} // class closed