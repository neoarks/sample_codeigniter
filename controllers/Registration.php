<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class registration extends CI_Controller {


	 /* @param: Class constructor
      * @description: Auto intialized on object creation
      * @author: gowithexpert
      * @copyrights: 
      */ 

	public function __construct() {
		parent::__construct();	
		$this->load->model('Registration_model');	

		if ( $this->session->userdata('user_name') === null ) { 
        	$allowed = array('index'); // Allowed registration function without login?
        	if ( ! in_array($this->router->fetch_method(), $allowed )) {
        		$this->session->set_flashdata('waring', 'Login requried'); 
            	redirect('login');
        	}
    	}
	} 


	 /* @param: Index Function 
      * @description: Function is used for rendering index (view html) 
      * @author: gowithexpert
      * @copyrights: 
      */ 
	 
	public function index() {
		$this->load->view('registration');
	}


	 /* @param: Employee registration 
      * @description: Function is used for employee Registration
      * @author: gowithexpert
      * @copyrights: 
      */ 

	public function emp_registration() {
		
		//Initialized variables for for registration values
		$this->first_name 	= $this->input->post('first_name');
		$this->last_name 	= $this->input->post('last_name');
		$this->birthday 	= $this->input->post('birthday');
		$this->mob 			= $this->input->post('mob');
		$this->dept 		= $this->input->post('dept');
		$this->email 		= $this->input->post('email');
		$this->pwd 			= md5($this->input->post('password'));
		
		//Server side validations
		if($this->first_name == '') {
			$this->session->set_flashdata('info', 'First name can not blank.!');
			redirect('login');
		}
		elseif($this->last_name == '') {
			$this->session->set_flashdata('info', 'Second name can not blank.!');
			redirect('login');
		}
		elseif($this->birthday == '') {
			$this->session->set_flashdata('info', 'Date of birth can not blank.!');
			redirect('login');
		}
		elseif($this->mob == '') {
			$this->session->set_flashdata('info', 'Mobile can not blank.!');
			redirect('login');
		}
		elseif($this->email == '') {
			$this->session->set_flashdata('info', 'Email can not blank.!');
			redirect('login');
		}
		elseif($this->dept == '') {
			$this->session->set_flashdata('info', 'Department can not blank.!');
			redirect('login');
		}
		elseif($this->pwd == '') {
			echo "Password is required !!!";
		}
		else {
			$this->registrationArr = array('emp_first_name' => $this->first_name,
											'emp_last_name'	=> $this->last_name ,
											'emp_mob'		=> $this->mob ,
											'emp_pwd'		=> $this->pwd ,
											'emp_email'		=> $this->email,
											'user_type'		=> 0,
											'emp_depts'		=> $this->dept 
											);

			$this->regStatus = $this->Registration_model->emp_reg($this->registrationArr);
			if($this->regStatus === true) {
				$this->session->set_tempdata('success', 'login Successfully.!', 3);
				//http://localhost/sis_spaceshare/index.php/success-flash
				//redirect('/success-flash');
			}
			else {
				$this->session->set_flashdata('error', 'Incorrect email for password');
				//echo "Registration Failed !!!";
			}

		}		

	} // the function ends here 


} // the end of class
